import asyncio
import uuid

from grpc import aio

import task_pb2
import task_pb2_grpc
from common import get_task


async def run():
    async with aio.insecure_channel("127.0.0.1:50051") as channel:
        stub = task_pb2_grpc.TaskServiceStub(channel)

        for _ in range(5):
            response = await stub.create(task_pb2.Task(
                id=str(uuid.uuid4()),
                procedure_id=str(uuid.uuid4()),
                source_node_id=str(uuid.uuid4()),
                target_node_id=str(uuid.uuid4()),
                schedule='test'
            ))
            print(f"created task: {get_task(response)}\n")

        response = await stub.get_list(task_pb2.Empty())

        tasks = [get_task(i) for i in response.items]
        print(f"tasks_list: {tasks}\n")

        response = stub.delete(task_pb2.Id(id=tasks[0]["id"]))
        print(f"deleted task: {tasks[0]['id']}\n")

        response = await stub.get_list(task_pb2.Empty())

        tasks = [get_task(i) for i in response.items]
        print(f"tasks_list: {tasks}\n")


if __name__ == '__main__':
    asyncio.run(run())
