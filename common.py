def get_task(item):
    return {
        "id": item.id,
        "procedure_id": item.procedure_id,
        "source_node_id": item.source_node_id,
        "target_node_id": item.target_node_id,
        "schedule": item.schedule
    }
