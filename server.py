import asyncio

from grpc import aio

import task_pb2
import task_pb2_grpc
from common import get_task


class Listener(task_pb2_grpc.TaskServiceServicer):
    def __init__(self, *args, **kwargs) -> None:
        self.items: dict = {}

    async def create(self, request, context):
        self.items[request.id] = get_task(request)

        return task_pb2.Task(**self.items[request.id])

    async def get(self, request, context):
        return task_pb2.Task(**self.items[request.id])

    async def get_list(self, request, context):
        return task_pb2.Tasks(items=self.items.values())

    async def delete(self, request, context):
        self.items.pop(request.id)
        return task_pb2.Empty()


async def run():
    server = aio.server()
    task_pb2_grpc.add_TaskServiceServicer_to_server(Listener(), server)
    server.add_insecure_port("[::]:50051")
    await server.start()
    await server.wait_for_termination()


if __name__ == '__main__':
    asyncio.run(run())
